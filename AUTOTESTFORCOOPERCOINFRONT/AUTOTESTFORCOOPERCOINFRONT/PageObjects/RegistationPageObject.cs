﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AUTOTESTFORCOOPERCOINFRONT.NewFolder
{
    public  class RegistationPageObject
    {
        //*[@id="root"]/div/div/div[2]/div[9]/div[2]/div[3]/div попап для удачн регистрации
        private static readonly By _goToRegistrationButton = By.XPath("//*[@id='login.link_to_registration']");
        private static readonly By _autorizationButton = By.XPath("//button[@id='registration.link_to_login']"); //div>div > a > button> label
        private static readonly By _firstNameField = By.XPath("//input[@id='registration.first_name_input']");
        private static readonly By _lastNameField = By.XPath("//input[@id='registration.last_name_input']");
        private static readonly By _loginField = By.XPath("//input[@id='registration.login_input']");
        private static readonly By _emailField = By.XPath("//input[@id='registration.email_input']");
        private static readonly By _phoneField = By.XPath("//input[@id='registration.phone_input']");
        private static readonly By _passwordField = By.XPath("//input[@id='registration.password_input']");
        private static readonly By _confirmPassField = By.XPath("//input[@id='registration.confirm_password_input']");//dodelat
        private static readonly By _submitButton = By.XPath("//button[@id='registration.registration_btn']");
        //private static readonly By _successfulregistration = By.XPath("//*[@id='root']/div/div/div[2]/div[9]/div[2]/div[3]/div");
        //public RegistationPageObject(IWebDriver webDriver)
        //{
        //    _webDriver = webDriver;
        //}
        internal static void OpenRegistration(IWebDriver _webDriver) => _webDriver.FindElement(_goToRegistrationButton).Click();
        public static void ClickAutorizationButton(IWebDriver _webDriver) => _webDriver.FindElement(_autorizationButton).Click();
        public static  void SetFirstName(IWebDriver _webDriver, string firstname) => _webDriver.FindElement(_firstNameField).SendKeys(firstname);
        public static void SetLastName(IWebDriver _webDriver, string lastname) => _webDriver.FindElement(_lastNameField).SendKeys(lastname);
        public static void SetLogin(IWebDriver _webDriver, string login) => _webDriver.FindElement(_loginField).SendKeys(login);
        public static void SetEmail(IWebDriver _webDriver, string email) => _webDriver.FindElement(_emailField).SendKeys(email);
        public static void SetPhone(IWebDriver _webDriver, string phone) => _webDriver.FindElement(_phoneField).SendKeys(phone);
        public static void SetPassword(IWebDriver _webDriver, string password) => _webDriver.FindElement(_passwordField).SendKeys(password);
        public static void SetConfirmPassword(IWebDriver _webDriver, string confirm) => _webDriver.FindElement(_confirmPassField).SendKeys(confirm);
        public static void ClickSubmitButton(IWebDriver _webDriver) => _webDriver.FindElement(_submitButton).Click();
        public static bool IsMassageFind(IWebDriver _webDriver, string result)
        {
            bool haveExept = false;
            if (_webDriver.FindElement(By.XPath(result)).Displayed)
            { haveExept = true; }
            return haveExept;
        }
    }
}
