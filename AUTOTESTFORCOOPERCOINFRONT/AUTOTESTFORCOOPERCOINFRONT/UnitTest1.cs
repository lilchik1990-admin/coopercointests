using AUTOTESTFORCOOPERCOINFRONT.NewFolder;
using AUTOTESTFORCOOPERCOINFRONT.Support;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.Extensions;

namespace AUTOTESTFORCOOPERCOINFRONT
{
    [TestFixture]
    [AllureNUnit]
    public class Tests
    {
        private static IWebDriver _webDriver;
        private const string firstName = "Test";
        private const string lastName = "Test";
        private const string login = "Test1610";
        private const string email = "test@test.test";
        private const string phone = "1111111111";
        private const string password = "Test1111";

        [OneTimeSetUp]
        public void Setup()
        {
            var option = new ChromeOptions();
            option.AddArguments("--start-maximized");
            _webDriver = new ChromeDriver(option);
            _webDriver.Navigate().GoToUrl("http://localhost:3000/login");
            RegistationPageObject.OpenRegistration(_webDriver);
        }
        [SetUp]
        public void OneTimeSetUp()
        {
            _webDriver.Navigate().GoToUrl("http://localhost:3000/login");
            RegistationPageObject.OpenRegistration(_webDriver);
        }

        [Test(Description = "First test run with Allure")]
        [AllureTag("CI")]
        [AllureSeverity(Allure.Commons.SeverityLevel.minor)]
        [AllureIssue("1")]
        [AllureTms("333")]
        [AllureOwner("Stiv")]
        public void ButtonIsWork() => RegistationPageObject.ClickAutorizationButton(_webDriver);
        [TestCase(firstName, lastName, login, email, phone, password, password, "//*[@id='root']/div/div/div[2]/div[9]/div[2]/div[3]/div")]
        public void ValidRegistration(string firstName, string lastName, string login, string email, string phone, string password, string confirm, string result)
        {

            RegistationPageObject.SetFirstName(_webDriver, firstName);
            RegistationPageObject.SetLastName(_webDriver, lastName);
            RegistationPageObject.SetLogin(_webDriver, login);
            RegistationPageObject.SetEmail(_webDriver, email);
            RegistationPageObject.SetPhone(_webDriver, phone);
            RegistationPageObject.SetPassword(_webDriver, password);
            RegistationPageObject.SetConfirmPassword(_webDriver, confirm);
            RegistationPageObject.ClickSubmitButton(_webDriver);
            Assert.IsTrue(RegistationPageObject.IsMassageFind(_webDriver, result));
        }
        [TestCase(firstName, lastName, login, email, phone, password, password, "//*[@id='root']/div/div/div[2]/h1[2]")]
        public void RegistrationWithSameData(string firstName, string lastName, string login, string email, string phone, string password, string confirm, string result)
        {
            RegistationPageObject.SetFirstName(_webDriver, firstName);
            RegistationPageObject.SetLastName(_webDriver, lastName);
            RegistationPageObject.SetLogin(_webDriver, login);
            RegistationPageObject.SetEmail(_webDriver, email);
            RegistationPageObject.SetPhone(_webDriver, phone);
            RegistationPageObject.SetPassword(_webDriver, password);
            RegistationPageObject.SetConfirmPassword(_webDriver, confirm);
            RegistationPageObject.ClickSubmitButton(_webDriver);
            Assert.IsTrue(RegistationPageObject.IsMassageFind(_webDriver, result));
            Assert.IsTrue(RegistationPageObject.IsMassageFind(_webDriver, result));
        }
        //[TestCase("", "Testttt", "Test1610", "test@test.test", "1111111111", "test111111", "test111111", "//*[@id='root']/div/div/div[2]/div[1]/div[2]/div")]
        //public void EmptyFirstName(IWebDriver webDriver, string firstName, string lastName, string login, string email, string phone, string password, string confirm, string result)
        //{
        //    //_webDriverForChrome.Manage()
        //    RegistationPageObject registation = new RegistationPageObject(webDriver);
        //   // webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        //    registation.SetFirstName(firstName);
        //    registation.SetLastName(lastName);
        //    registation.SetLogin(login);
        //    registation.SetEmail(email);
        //    registation.SetPhone(phone);
        //    registation.SetPassword(password);
        //    registation.SetConfirmPassword(confirm);
        //    registation.ClickSubmitButton();
        //    Assert.IsTrue(registation.IsMassageFind(result));
        //}
        //[TestCase("Testttt", "", "Test1610", "test@test.test", "1111111111", "test111111", "test111111", "//*[@id='root']/div/div/div[2]/div[2]/div[2]/div")]
        //public void EmptyLastName(IWebDriver webDriver, string firstName, string lastName, string login, string email, string phone, string password, string confirm, string result)
        //{
        //    //_webDriverForChrome.Manage()
        //    RegistationPageObject registation = new RegistationPageObject(webDriver);
        //  //  webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        //    registation.SetFirstName(firstName);
        //    registation.SetLastName(lastName);
        //    registation.SetLogin(login);
        //    registation.SetEmail(email);
        //    registation.SetPhone(phone);
        //    registation.SetPassword(password);
        //    registation.SetConfirmPassword(confirm);
        //    registation.ClickSubmitButton();
        //    Assert.IsTrue(registation.IsMassageFind(result));
        //}
        //[TestCase("Testttt", "Testttt", "", "test@test.test", "1111111111", "test111111", "test111111", "//*[@id='root']/div/div/div[2]/div[2]/div[2]/div")]
        //public void EmptyLogin(IWebDriver webDriver, string firstName, string lastName, string login, string email, string phone, string password, string confirm, string result)
        //{
        //    RegistationPageObject registation = new RegistationPageObject(webDriver);
        // //   _webDriverForChrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        //    registation.SetFirstName(firstName);
        //    registation.SetLastName(lastName);
        //    registation.SetLogin(login);
        //    registation.SetEmail(email);
        //    registation.SetPhone(phone);
        //    registation.SetPassword(password);
        //    registation.SetConfirmPassword(confirm);
        //    registation.ClickSubmitButton();
        //    Assert.IsTrue(registation.IsMassageFind(result));
        //}
        //[TestCase("Testttt", "Testttt", "Test1610", "", "1111111111", "test111111", "test111111", "//*[@id='root']/div/div/div[2]/div[2]/div[2]/div")]
        //public void EmptyEmail(IWebDriver webDriver, string firstName, string lastName, string login, string email, string phone, string password, string confirm, string result)
        //{
        //    RegistationPageObject registation = new RegistationPageObject(webDriver);
        //  //  _webDriverForChrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        //    registation.SetFirstName(firstName);
        //    registation.SetLastName(lastName);
        //    registation.SetLogin(login);
        //    registation.SetEmail(email);
        //    registation.SetPhone(phone);
        //    registation.SetPassword(password);
        //    registation.SetConfirmPassword(confirm);
        //    registation.ClickSubmitButton();
        //    Assert.IsTrue(registation.IsMassageFind(result));
        //}
        //[TestCase("Testttt", "Testttt", "Test1610", "test@test.test", "", "test111111", "test111111", "//*[@id='root']/div/div/div[2]/div[5]/div[2]/div")]
        //public void EmptyPhone(IWebDriver webDriver, string firstName, string lastName, string login, string email, string phone, string password, string confirm, string result)
        //{
        //    RegistationPageObject registation = new RegistationPageObject(webDriver);
        //  //  _webDriverForChrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        //    registation.SetFirstName(firstName);
        //    registation.SetLastName(lastName);
        //    registation.SetLogin(login);
        //    registation.SetEmail(email);
        //    registation.SetPhone(phone);
        //    registation.SetPassword(password);
        //    registation.SetConfirmPassword(confirm);
        //    registation.ClickSubmitButton();
        //    Assert.IsTrue(registation.IsMassageFind(result));
        //}
        //[TearDown]
        //public void TearDown()
        //{
        //    Supports.CleanDriver();
        //}
        //[OneTimeTearDown]
        //public void OneTimeTearDown()
        //{
        //    Supports.CloseDriver();
        //}
        //[Test]
        //public void TestAutorization()
        //{
        //    //_webDriverForChrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        //    //var registration = new RegistationPageObject(_webDriverForChrome);
        //    //registration.ClickAutorizationButton();
        //    //Thread.Sleep(3000);
        //    //Assert.
        //}\
        //[TearDown]
        //public void TearDown()
        //{
        //    _webDriverForChrome.Quit();
        //}
        [TearDown]
        public void TearDown()
        {
            Supports.CleanDriver(_webDriver);
        }
        [OneTimeTearDown]
        public void OneTimeTear()
        {
            Supports.CloseDriver(_webDriver);
        }
    }
}