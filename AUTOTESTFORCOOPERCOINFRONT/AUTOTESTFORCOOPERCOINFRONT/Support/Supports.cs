﻿using AUTOTESTFORCOOPERCOINFRONT.NewFolder;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.Extensions;
using System;

namespace AUTOTESTFORCOOPERCOINFRONT.Support
{
    public static class Supports
    {
        //private static IWebDriver _driver;

        public static void CreateDriver(IWebDriver _driver)
        {
            var option = new ChromeOptions();
            option.AddArguments("--start-maximized");
            _driver = new ChromeDriver(option);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
        }
        internal static IWebDriver GetDriver(IWebDriver _driver)
        {
            if (_driver == null)
                CreateDriver(_driver);

            return _driver;
        }
        public static void CleanDriver(IWebDriver _driver)
        {
            // Open new empty tab.
            _driver.ExecuteJavaScript("window.open('');");

            // Close all tabs but one tab and delete all cookies.
            for (var tabs = _driver.WindowHandles; tabs.Count > 1; tabs = _driver.WindowHandles)
            {
                _driver.SwitchTo().Window(tabs[0]);
                _driver.Manage().Cookies.DeleteAllCookies();
                _driver.Close();
            }

            // Switch to empty tab.
            _driver.SwitchTo().Window(_driver.WindowHandles[0]);
        }
        internal static void CloseDriver(IWebDriver _driver)
        {
            _driver.Quit();
        }
    }
}
